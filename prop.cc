#include<iostream>
#include<vector>
#include<string>
#include<sstream>
using namespace std;

int main()
{
     string input;
     while(getline(cin,input))
     {
        if(input == "q") break;
        istringstream ss{input};
        vector<string> st;
        while(!ss.eof())
       {
           string s; ss >> s; string p,p1,p2,p3;
           if((s == "1") || (s == "0"))
	   {
	      st.push_back(s);
	   }
	   else if(s == "not")
	   {
	     p = st.back(); st.pop_back();
	     if(p == "1") p3 = "0";
	     else p3 = "1";
             st.push_back(p3);
	   }
	   else if(s == "and")
	   {
	     
	     p1 = st.back();
	     st.pop_back();
	     p2 = st.back();
	     st.pop_back();
	     if((p1 == "1") && (p2 == "1"))
	     {
	        p3 = "1"; 
	     }
	     else 
	     {
	        p3 = "0"; 

             }
	     st.push_back(p3);
	   }
	   if(s == "or")
	   {
	     p1 = st.back();
	     st.pop_back();
	     p2 = st.back();
	     st.pop_back();
	     if((p1 == "1") || (p2 == "1"))
	     {
	        p3 = "1";
	     }
	     else
	     {
	       p3 = "0";
	     }
	     st.push_back(p3);
	   }
	   if(s == "->")
	   {
	      p1 = st.back();
	      st.pop_back();
	      p2 = st.back();
	      st.pop_back();
	      if((p1 == "1") || (p2 == "0"))
	      {
	        p3 = "1";
	      }
	      else
	      {
	        p3 = "0";
	      }
	      st.push_back(p3);
	   }
	   if(s == "<->")
	   {
	      p1 = st.back();
	      st.pop_back();
	      p2 = st.back();
              st.pop_back();
	      if(p1 == p2)
	      {
	          p3 = "1";
	      }
	      else
	      {
	         p3 = "0";
	      }
	      st.push_back(p3);
	   }
       }
        
        cout << st.back() <<endl;
     }
     
}
